---
id: userdocs_video_share
guide: userdocs_video_share
layout: userguide
additional_reading_tags: ["channel", "profile"]
---

To share a video, you first have to go to the video page you want to share. If you are the publisher, you can list all your videos via the left-menu three dots option _My Videos_. Once on the video page, you just have to click the _Share_ button, and are presented with a few options:

![Modal presenting options to share a video](/assets/video-share_modal.png){:class="img-fluid"}

1.  a URL to the video, i.e.: `https://framatube.org/videos/watch/9db9f3f1-9b54-44ed-9e91-461d262d2205`. This address can be sent to your contact the way you want ; they will be able to access the video directly.
1.  an embed code that allows you to insert a video player in your website.

**Tip** : the small icones at the end of the line allow you to copy the whole URL at once.

## Download a video <a class="toc" id="toc-download-a-video" href="#toc-download-a-video"></a>

You can download videos directly from the web interface of PeerTube, by clicking the dot button "..." aside the title, and then the "Download" option. A choice is then given to you regarding how you want to download:

![Modal presenting options to download a video](/assets/video-share_download.png){:class="img-fluid"}

* "Direct Download", which does what it says: your web browser downloads the video from the origin server of the video.
* peer-to-peer download via "Torrent", where you need a WebTorrent compatible client to download the video not only from the origin server but also from other peers watching the video or sharing it from their own WebTorrent-compatible clients at home! (by doing so you help the network be more resilient!) - any BitTorrent can also download the video, but without WebTorrent support they won't be able to exchange with web browsers.

**Tip** : depending on the instance, you can download the video in different formats. However, please make sure you are granted a licence compatible with the planned usage of the video beforehand.

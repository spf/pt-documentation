<div class="alert alert-warning" role="alert">
  <strong>Warning:</strong> This guide assumes that you have read the instructions in
<a href="https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md">production.md</a>
and thus omits some configurations steps (reverse proxy, database configuration, etc.). At
the end you should have a PeerTube instance running the latest development version, which can
be considered unstable.
</div>

First, go to the Peertube folder and switch to the Peertube user.

```bash
cd /var/www/peertube/versions/
sudo -u peertube -H bash
```

Then clone the git repository.

```bash
git clone https://github.com/Chocobozzz/PeerTube.git peertube-develop
cd peertube-develop/
```

It should automatically be on the `develop` branch, which you can verify with `git branch`. You can
also switch to another branch or a specific commit with `git checkout [branch or commit]`. Once you
have the correct version, run the build:

```bash
yarn install --pure-lockfile
npm run build
```

The compilation will take a long time. You can also run it on your local computer, and transfer the
entire folder to your server.

Now you should make sure to add any new config fields to your `production.yaml`. And you should make
a backup of the database:

```bash
SQL_BACKUP_PATH="backup/sql-peertube_prod-$(date -Im).bak" && \
     cd /var/www/peertube && sudo -u peertube mkdir -p backup && \
     sudo -u postgres pg_dump -F c peertube_prod | sudo -u peertube tee "$SQL_BACKUP_PATH" >/dev/null
```

Finally, update the `peertube-latest` symlink to point at the new version:

```bash
cd /var/www/peertube && \
    sudo unlink ./peertube-latest && \
    sudo -u peertube ln -s versions/peertube-develop ./peertube-latest
```

Now you just need to restart Peertube. With systemd, just run `sudo systemctl restart peertube`.

Do not try to upgrade from one development version to another by running `git pull` and `npm run build`. This
will break your website. Either switch back to a release version, or make a copy of the `peertube-develop`
folder and run the compilation there.

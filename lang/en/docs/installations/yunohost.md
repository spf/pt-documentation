<div class="install-only-beta" markdown="1">
On Debian running YunoHost, you can install Yarn, Node and PeerTube in one shot via a __community package__.

[![Install Peertube with YunoHost](https://install-app.yunohost.org/install-with-yunohost.png)](https://install-app.yunohost.org/?app=peertube)

See [here](https://github.com/YunoHost-Apps/peertube_ynh) for support.

</div>

<div class="install-only-rc install-only-nightly" markdown="1">
Currently, there are no Debian packages available for RC or nightly builds of PeerTube. Please use the [production guide](https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md).
</div>

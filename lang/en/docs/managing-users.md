---
id: docs_managing_users
guide: docs_configuration
layout: guide
---

## Users <a class="toc" id="toc-users" href="#toc-users"></a>

The user menu allows you to manage all existing users on your instance.

![Window displayed when in user menu](/assets/managing_users_menu.png){:class="img-fluid"}

Users will be created when they register to your instance, but you may also create
users manually using the "create user" button at the top right of the user menu.

### Manage users <a class="toc" id="toc-manage-users" href="#toc-manage-users"></a>

Under the user menu you can update or delete a user by clicking on the three dots at the right of a user info.

![User dot button](/assets/managing_users_dots_button.png){:class="img-fluid"}

- _Edit_ will allow you to update user information - see below.
- _Delete_ will allow you to definitely delete this user. **All his videos will also being deleted**.
- _Ban_ will disable connection for this user and remove their videos. No one else will be able to register with the same nickname or email address.

### Editing users <a class="toc" id="toc-editing-users" href="#toc-editing-users"></a>

When clicking on _Edit_, the edition window appears.
Here you can update parameters for a user, privileges they can use, their quota and so on.

![Window displayed when clicking on Edit user](/assets/managing_users_edit_menu.png){:class="img-fluid"}

**Role** defines what a user is allowed to do on the instance.

- Administrators can do anything, and have access to the full admin backend. If you are editing users, then you are an administrator :-)
- Moderators are able to read reports other users do, and ban videos. They have access to the "Moderation" part of the administration backend which Administrators also see.
- User is the default role. They can upload (within limit of their quota), see and comment videos.

**Video quota** represents the size limit a user cannot exceed when uploading videos. Each time a user upload a video, Peertube checks if there is enough quota to store it. If not, the upload is denied.
Beware, the quota after an upload is estimated only on the size of the file uploaded. However, after transcoding (which outputs videos of unpredictable size) the video resolutions resulting of the transcoding are also taken into account in a user's quota. If you have enabled multiple resolutions, a user can use more than their quota on disk. Peertube will provide you a estimation of maximal space a user will use according to your transcoding options.
You can change the default for new users in the configuration menu.

**Video daily quota** represents the max quota a user is allowed to upload by day. You can tune this parameter to adjust the resources your instance will use.
For instance, if you have many users transcoding could take a long time, so limiting upload by user by day could help to share resources between them.
You can change the default for new users in the configuration menu.

Once you are satisfied with your user, you can click on the "Update User" button to save modifications.

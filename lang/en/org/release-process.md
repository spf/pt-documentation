---
id: release_process
guide: peertube_organization
layout: guide
timer: false
---

## Release schedule <a class="toc" id="toc-release-schedule" href="#toc-release-schedule"></a>

In october 2018, the we published our first v1.0.0. It is considered a basis stable enough to be broadly used and the main mechanics are in place. Not all use cases are covered and edges are still rough, but it should be pretty usable nonetheless.

While year 2018 followed a fast pace of releases for each of the crowdfunding's features, 2019 should see a more natural rythm of release. Expect regular `v1.x.x` versions, which follow [semantic versioning](https://semver.org/).
